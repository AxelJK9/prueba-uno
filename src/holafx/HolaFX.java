
package holafx;

import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.Scene;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 *
 * @author Axel Tamayo
 */
public class HolaFX extends Application {
    
    Label selectionMsg = new Label("Selecciona tu auto:");
    
    @Override
    public void start (Stage stage) {
        
        CheckBox fordCbx = new CheckBox("Ford");
        CheckBox audiCbx = new CheckBox("Audi");
        audiCbx.setAllowIndeterminate(true);
        
        fordCbx.selectedProperty().addListener(new ChangeListener<Boolean>(){
            public void changed(ObservableValue<? extends Boolean> ov, final Boolean value, final Boolean newValue){
                if(newValue != null && newValue){   
                    System.out.println("Ford seleccionado");
                }
            }
        });
            
        
        VBox root = new VBox();
        root.getChildren().addAll(selectionMsg, fordCbx, audiCbx);
        root.setSpacing(20);
        root.setMinSize(350, 250);
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.setTitle("Un ejemplo de CheckBox");
        stage.show();
        
        }
                
    
 
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
}
