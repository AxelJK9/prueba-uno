import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.Label;
import javafx.stage.Stage;
import javafx.scene.layout.VBox;
import javafx.scene.control.CheckBox;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.HBox;
/**
 *
 * @author Axel Tamayo
 */
public class Taller extends Application {
    @Override
    public void start(Stage primaryStage) {
        TextField textField = new TextField();
        TextField textField1 = new TextField();
        CheckBox checkbox = new CheckBox("Moda");
        CheckBox checkbox1 = new CheckBox("Tecnologia");
        CheckBox checkbox2 = new CheckBox("Farandula");
        CheckBox checkbox3 = new CheckBox("Arte");
        RadioButton radiobutton = new RadioButton("Masculino");
        RadioButton radiobutton1 = new RadioButton("Femenino");
        Button closeButton = new Button("Guardar");
        
        
        Label selectionMsg = new Label("Selecciona tu sexo:");
        Label label1 = new Label("Nombres:");
        Label label2 = new Label("Apellidos:");
        Label label3 = new Label("");
        Label label4 = new Label("");
        Label label5 = new Label("");
        Label label6 = new Label("");
        Label label7 = new Label("");
        Label label8 = new Label("");
        Label label9 = new Label("");
        

        VBox vbox = new VBox(label1, label2, label3, label4, label5, label6, label7, label8, label9);
        vbox.setSpacing (20);
        label1.setGraphic(textField);
        label1.setContentDisplay(ContentDisplay.RIGHT);
        label2.setGraphic(textField1);
        label2.setContentDisplay(ContentDisplay.RIGHT);
        label3.setGraphic(radiobutton);
        label3.setContentDisplay(ContentDisplay.RIGHT);;
        label4.setGraphic(radiobutton1);
        label4.setContentDisplay(ContentDisplay.RIGHT);
        label5.setGraphic(checkbox);
        label5.setContentDisplay(ContentDisplay.RIGHT);
        label6.setGraphic(checkbox1);
        label6.setContentDisplay(ContentDisplay.RIGHT);
        label7.setGraphic(checkbox2);
        label7.setContentDisplay(ContentDisplay.RIGHT);
        label8.setGraphic(checkbox3);
        label8.setContentDisplay(ContentDisplay.RIGHT);
        label9.setGraphic(closeButton);
        label9.setContentDisplay(ContentDisplay.RIGHT);
        

        ToggleGroup group = new ToggleGroup();
        // Add all RadioButtons to a ToggleGroup
        group.getToggles().addAll(radiobutton, radiobutton1);

        group.selectedToggleProperty().addListener(new ChangeListener<Toggle>(){
            public void changed(ObservableValue<? extends Toggle> ov, final Toggle toggle, final Toggle new_toggle){
                String toggleBtn = ((ToggleButton)new_toggle).getText();
                selectionMsg.setText("Your selection: " + toggleBtn);
        }});

        
        // Select the default car as ferrari
        radiobutton.setSelected(true);
        // Create the Selection Label
        Label msg = new Label("Select the car you like the most:");
        // Create a HBox
        HBox buttonBox = new HBox();
        // Add RadioButtons to an HBox
        buttonBox.getChildren().addAll(radiobutton, radiobutton1);
        // Set the spacing between children to 10px
        buttonBox.setSpacing(10);
        // Create the VBox
        VBox root = new VBox();
        // Add Labels and RadioButtons to an VBox
        root.getChildren().addAll(selectionMsg, msg, buttonBox);
        // Set the spacing between children to 10px
        root.setSpacing(10);
        // Set the Size of the VBox
        root.setMinSize(350, 250);
        
        
        
        
        Scene scene = new Scene(vbox, 500, 400);     
        primaryStage.setScene(scene);
        primaryStage.show();
    }
    public static void main(String[] args) {
        launch(args);
    }
}
